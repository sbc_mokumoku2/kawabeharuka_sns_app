require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get html" do
    get home_html_url
    assert_response :success
  end

  test "should get erd" do
    get home_erd_url
    assert_response :success
  end
end
